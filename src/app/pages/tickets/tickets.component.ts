import { Component, OnInit, OnDestroy } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import { ApiService } from '../../services/api.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss']
})
export class TicketsComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();
  public type: number = 0;
  data:any;
  displayedColumns: string[] = ['title', 'description','accion'];
  dataSource = new MatTableDataSource();
  pageSize = 10;
  //color: ThemePalette = 'primary';
  constructor(private _apiService: ApiService) { }

  ngOnInit(): void {
    this.getTickets();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getTickets(){
    this._apiService.getTickets().pipe(takeUntil(this.unsubscribe$)).subscribe((result:any)=>{
      this.dataSource.data = result.payload.data;
    },(e) => {
      this.dataSource.data = [];
    })
  }

  openModalAdd() {
    this.type = 1;
  }

  openModalEdit(element:any) {
    this.data = element;
    this.type = 2;
  }


  onDeleteTickets(element:any) {
    const id=element._id;
    this._apiService.deleteTickets(id).pipe(takeUntil(this.unsubscribe$)).subscribe(result=>{
      if(result.code === 200) {
        this.getTickets();
      }
    },(e)=>{
      this.getTickets();
    })
  }

  onSubmit(event:any) {
    if(event){
      this.getTickets();
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
