import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TicketsRoutingModule } from './tickets-routing.module';
import { TicketsComponent } from './tickets.component';
import { MaterialUiModule } from '../../modules/material-ui/material-ui.module';
import { SharedModulesModule } from '../../modules/shared-modules/shared-modules.module';
import { ModalComponent } from '../../components/modal/modal.component';
import {MatTableModule} from '@angular/material/table';
import {CdkTableModule} from '@angular/cdk/table';
import {MatPaginatorModule} from '@angular/material/paginator';

@NgModule({
  declarations: [
    TicketsComponent,
    ModalComponent,
  ],
  imports: [
    CommonModule,
    TicketsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModulesModule,
    MatTableModule,
    CdkTableModule,
    MatPaginatorModule,
    MaterialUiModule
  ]
})
export class TicketsModule { }
