import { Component, OnInit, OnDestroy  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../services/auth.service';
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss', '../auth.component.scss'],
})
export class SignInComponent implements OnInit, OnDestroy {
  subscription: Subscription | any;
  hide = true;
  form: FormGroup;
  public messageError: string = '';
  public messageErrorStatus: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private _authService: AuthService
  ) {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      contrasena: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onCloseAlert() {
    this.messageErrorStatus = false;
  }
  onSignIn() {
    const value = this.form.value;
    this._authService.singIn(value).subscribe(
      (result) => {
        this._authService.saveToken(result.payload.token);
        this.router.navigate(['/']);
      },
      (e) => {
        this.messageError = e.error.message;
        this.messageErrorStatus = true;
        setTimeout(()=>{
          this.messageError = '';
          this.messageErrorStatus = false;
        },4000)
      }
    );
  }
}
