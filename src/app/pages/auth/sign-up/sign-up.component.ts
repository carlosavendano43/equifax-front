import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../services/auth.service';
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss','../auth.component.scss']
})
export class SignUpComponent implements OnInit, OnDestroy {
  subscription: Subscription | any;
  hide = true;
  form: FormGroup;
  public messageError: string = '';
  public messageErrorStatus: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private _authService: AuthService
  ) {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      contrasena: ['', [Validators.required, Validators.minLength(6)]],
    });
   }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onCloseAlert() {
    this.messageErrorStatus = false;
  }
  onSignUp() {
    const value = this.form.value;
    this.subscription = this._authService.singUp(value).subscribe(
      (result) => {
    
        setTimeout(()=> {
          this.router.navigate(['/auth/sign-in']);
        },5000)
        
      },
      (e) => {
        this.messageError = e.error.message;
        this.messageErrorStatus = true;
      }
    );
  }

}
