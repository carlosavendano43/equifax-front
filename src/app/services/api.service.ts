import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Response } from '../interface/response';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  api: string = `${environment.api}/tickets`;
  constructor(
    private http: HttpClient,
  ) { }

  getTickets(id?:string) {
    const url = id === undefined ? this.api : `${this.api}?id=${id}`;
    return this.http.get(url);
  }

  postTickets(body:any) {
    const url = `${this.api}/register`;
    return this.http.post<Response>(url,body);
  }

  putTickets(body:any) {
    const url = `${this.api}/modify`;
    return this.http.put<Response>(url,body);
  }
  deleteTickets(id:string) {
    const url = `${this.api}/delete/${id}`;
    return this.http.delete<Response>(url);
  }
}
