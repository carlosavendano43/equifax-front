import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  api: string = `${environment.api}/auth`;
  constructor(
    private http: HttpClient,
    private router: Router
  ) { }
  getToken():any{
    return <any>sessionStorage.getItem('user');
  }
  singIn(Data:any){
    const path=`${this.api}/sign-in`;
    return this.http.post<any>(path,Data);
  }
  singUp(Data:any){
    const path=`${this.api}/sign-up`;
    return this.http.post<any>(path,Data);
  }

  logout(){
    sessionStorage.removeItem('user');
    this.router.navigate(['/auth/sign-in']);
  }

  saveToken(user:string){
    sessionStorage.setItem('user',user);
  }

  loggedIn():Boolean{
    return !!sessionStorage.getItem('user');
  }
}
