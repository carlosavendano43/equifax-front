import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../services/api.service';
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit,OnChanges {
  @Input() type: number = 0;
  @Input() data: any;
  @Output() onSubmitEvent=new EventEmitter<any>();
  public title: string = '';
  public titleBtn: string = '';

  form: FormGroup;
  public messageError: String = '';
  public messageErrorStatus: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private _apiService: ApiService
  ) {
    this.form = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required, Validators.minLength(5)]],
      status: [false, [Validators.required]],
    });
  }

  ngOnInit(): void {
    
  }
  ngOnChanges() {
    this.onIniModal();
  }

  onIniModal() {
    if(this.type !== 0) {
      this.title =  this.type === 1 ? 'Agregar Tickets' : 'Modificar Tickets';
      this.titleBtn = this.type === 1 ? 'Registrar' : ' Modificar';
      if(this.type === 2) {
        this.form.patchValue({
          title:this.data.title,
          description:this.data.description,
          status:this.data.status
        })
      }
    }
  } 

  onPostTickest() {
    const value = this.form.value;

    this._apiService.postTickets(value).subscribe(result => {
        if(result.code === 200) {
          this.onSubmitEvent.emit(true);
          this.clearForm();
        }
      },
      (e) => {
        this.messageError = e.error.message;
        this.messageErrorStatus = true;
      }
    );
  }

  onPutTickest() {
    let value = this.form.value;
    value.id = this.data._id;

    this._apiService.putTickets(value).subscribe(result => {
      if(result.code === 200) {
        this.onSubmitEvent.emit(true);
        this.clearForm();
      }
      },(e) => {
        this.messageError = e.error.message;
        this.messageErrorStatus = true;
      }
    );
  }

  clearForm() {
    this.form.patchValue({
      title:'',
      description:'',
      status:false
    });
  }
}
