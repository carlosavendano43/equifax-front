import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss', '../styled.scss']
})
export class HeaderComponent implements OnInit, OnChanges {
  userState: Boolean = false;
  constructor(
    private authService:AuthService,
    private router:Router,
    ) { }

  ngOnInit(): void {
    this.userState = this.authService.loggedIn();
  }

  ngOnChanges(changes: SimpleChanges): void {
      
  }

  logout() {
    this.authService.logout();
  }

  goToSignUp () {
    this.router.navigate(['/auth/sign-up'])
  }

  goToSignIn () {
    this.router.navigate(['/auth/sign-in'])
  }

}
